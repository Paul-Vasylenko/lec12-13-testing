import CartParser from './CartParser';

let parser, calcTotal, parseLine, readFile, validate, parse;

beforeEach(() => {
    parser = new CartParser();
    calcTotal = parser.calcTotal.bind(parser);
    parseLine = parser.parseLine.bind(parser);
    readFile = parser.readFile.bind(parser);
    validate = parser.validate.bind(parser);
    parse = parser.parse.bind(parser);
});

describe('CartParser - tests', () => {
    describe('calcTotal', () => {
        it('should return correct calculations result.', () => {
            const cartItems = [
                {
                    name: 'Item1',
                    price: 10.5,
                    quantity: 2,
                },
                {
                    name: 'Item2',
                    price: 70,
                    quantity: 1,
                },
                {
                    name: 'Item3',
                    price: 14,
                    quantity: 5,
                },
            ];

            expect(calcTotal(cartItems)).toEqual(161);
        });
    });

    describe('parseLine', () => {
        it('should return an object with keys id, name, price, quantity and correct values.', () => {
            const resultObject = parseLine('Scelerisque lacinia,18.90,1');
            expect(resultObject).toMatchObject({
                id: expect.any(String),
                name: expect.any(String),
                price: expect.any(Number),
                quantity: expect.any(Number),
            });
            expect(resultObject.name).toEqual('Scelerisque lacinia');
            expect(resultObject.price).toEqual(18.9);
            expect(resultObject.quantity).toEqual(1);
        });
    });

    describe('validate', () => {
        it('should send error if file has unexpected columns.', () => {
            const contents = `Product name,Hello,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,18.90,1
			Consectetur adipiscing,28.72,10
			Condimentum aliquet,13.90,1`;

            const validationResult = validate(contents);

            expect(validationResult.length).toBeGreaterThan(0);
        });

        it('should send error if file has unexpected number of columns in rows', () => {
            const contents = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,1
			Consectetur adipiscing
			Condimentum aliquet,13.90,1`;

            const validationResult = validate(contents);

            expect(validationResult.length).toBeGreaterThan(0);
        });

        it("should send error if file has empty string in 'Product name' field", () => {
            const contents = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			,10.32,1
			Scelerisque lacinia,18.90,1
			Consectetur adipiscing,28.72,10
			,13.90,1`;

            const validationResult = validate(contents);

            expect(validationResult.length).toBeGreaterThan(0);
        });

        it("should send error if file has NaN or <0 number in 'Price' or 'Quantity' field", () => {
            const contents = `Product name,Price,Quantity
			Mollis consequat,-9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,true,1
			Consectetur adipiscing,28.72,hello
			Condimentum aliquet,13.90,1`;

            const validationResult = validate(contents);

            expect(validationResult.length).toBeGreaterThan(0);
        });

        it('should return empty array if the file is correct', () => {
            const contents = `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,7.50,1
			Consectetur adipiscing,28.72,4
			Condimentum aliquet,13.90,1`;

            const validationResult = validate(contents);

            expect(validationResult.length).toEqual(0);
        });
    });

    describe('parse', () => {
        it('should not try to get result of bad-validated file', () => {
            try {
                const path = 'samples/badfile.csv';
                parse(path);
                expect(true).toBe(false);
            } catch (e) {
                expect(e.message).toBe('Validation failed!');
            }
        });
    });
    describe('readFile', () => {
        it('should send error if file in directory does not exist', () => {
            const filePath = 'samples/notExistingFile.csv';
            try {
                parse(filePath);
                expect(true).toBe(false);
            } catch (e) {
                expect(e.message).toEqual(
                    `ENOENT: no such file or directory, open '${filePath}'`,
                );
            }
        });
        it('should return a string if a file exists', () => {
            const filePath = 'samples/goodfile.csv';
            const fileBody = readFile(filePath);
            expect(typeof fileBody).toBe('string');
        });
    });
});

describe('CartParser - integration test', () => {
    describe('parse', () => {
        it('should parse a file and return object-JSON', () => {
            const filePath = 'samples/integrationTest.csv';
            const resultObject = parse(filePath);
            const expectedObject = {
                items: [
                    {
                        name: 'Mollis consequat',
                        price: 9,
                        quantity: 2,
                    },
                    {
                        name: 'Tvoluptatem',
                        price: 10,
                        quantity: 5,
                    },
                ],
                total: 68,
            };
            expect(resultObject).toMatchObject(expectedObject);
        });
    });
});
